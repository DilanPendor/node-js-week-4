const nodemailer = require('nodemailer');
const sgTransport = require('@sendgrid/mail');

let mailer;
if(process.env.NODE_ENV==='production'){
    sgTransport.setApiKey(process.env.SENDGRID_API_KEY)
    mailer = sgTransport;
}else{
    mailConfig ={
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'beulah.mertz88@ethereal.email',
            pass: 'U25jDX5Kd3HWEKeN75'
        }
    }
    mailer = nodemailer.createTransport(mailConfig);
}

module.exports = mailer;
