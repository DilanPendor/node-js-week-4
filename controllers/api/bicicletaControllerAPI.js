const { allBicis } = require('../../models/bicicleta')
var Bicicleta = require('../../models/bicicleta')

exports.bicicleta_list = function(req, res){
    var allBicis;
    Bicicleta.allBicis(function(err,bicis){
        allBicis = bicis;
        res.status(200).json({
            bicicletas: allBicis
        })
    });
}

exports.bicicleta_create = function(req,res){
    var bici = new Bicicleta({code: req.body.code, color:req.body.color, modelo:req.body.modelo,ubicacion:[req.body.lat,req.body.lng]})
    Bicicleta.add(bici);

    res.status(201).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req,res){
    Bicicleta.removeByCode(req.body.code,function(err){
        res.status(204).send();
    });
}

exports.bicicleta_update = function(req,res){
    Bicicleta.findByCode(req.body.code,function(err,tBici){
        tBici.color = req.body.color;
        tBici.modelo = req.body.modelo
        tBici.ubicacion = [req.body.lat, req.body.lng];
        tBici.save();
        res.status(200).json({
            bicicleta: tBici
        });
    });
}