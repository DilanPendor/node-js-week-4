var mongooseAPI = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario')
var Reserva = require('../../models/reserva');

describe ('Testing Ususario | ', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongooseAPI.connect(mongoDB, {useNewUrlParser: true});

        const db = mongooseAPI.connection;
        db.on('error',console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function(){
            console.log('We are connected to the test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err,success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err,success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err,success){
                    if(err) console.log(err);
                    mongooseAPI.disconnect(err); 
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva la bici - ',() => {
        it('Debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: "Ezequiel"});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color:'verde', modelo: 'Urbana', ubicacion:[4, -74]})
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, manana, function(err,reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                })
            });
        });
    });
});